import 'package:hexcolor/hexcolor.dart';

class ColorsSchool {
  static final primaryColor = HexColor('283845');
  static final secondaryColor = HexColor('B8B08D');
  static final thirdColor = HexColor('F2D492');
  static final fourthColor = HexColor('F29559');
  static final fifthColor = HexColor('F4F4F9');
}